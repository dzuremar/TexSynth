#include <string>
#include <curand.h>
#include <curand_kernel.h>

#include "Patchmatch/jzq.h"
#include "Patchmatch/imageio.h"
#include "Patchmatch/texarray2.h"
#include "Patchmatch/patchmatch_gpu.h"
#include "Patchmatch/timer.h"

#define TEXSYNTH_DEBUG 0

#if TEXSYNTH_DEBUG
typedef Vec<1, int> V1i;
typedef Array2<Vec<1, int>> A2V1i;
#endif

// assigns random pattern coordinates to all patches (ignoring specific area near texture borders to ensure that patches won't get clamped)
__global__ void initRandomPatches(TexArray2<2, int> patches, unsigned int divWidth, unsigned int divHeight, unsigned int offset, curandState * randStates)
{
    int x = blockIdx.x;
    int y = blockIdx.y;

    if (x >= patches.width || y >= patches.height)
        return;

    // pick random patch for coordinates [x;y]
    int ord = gridDim.x * y + x;
    int patchX = (int)(offset + curand(&randStates[ord]) / divWidth);
    int patchY = (int)(offset + curand(&randStates[ord]) / divHeight);
    patches.write(x, y, Vec2i(patchX, patchY));
}

// clamps value to interval
__host__ __device__ int cudaClamp(int value, int min, int max)
{
    value = (value < min) ? min : value;
    value = (value > max) ? max : value;
    return value;
}

// evaluates average pixel value over all pattern pixels that are targeted by patches that touch the pixel
__global__ void adjustTexture(TexArray2<4, unsigned char> texture, TexArray2<4, unsigned char> pattern, TexArray2<2, int> patches, int patchRadius)
{
    int x = blockIdx.x;
    int y = blockIdx.y;

    if (x >= texture.width || y >= texture.height)
        return;

    // clamp patch intervals
    int minX = cudaClamp(x - patchRadius, 0, patches.width - 1);
    int maxX = cudaClamp(x + patchRadius, 0, patches.width - 1);
    int minY = cudaClamp(y - patchRadius, 0, patches.height - 1);
    int maxY = cudaClamp(y + patchRadius, 0, patches.height - 1);

    // accumulate all pattern pixels that correspond to [x;y] in the texture
    V3f acc(0.f, 0.f, 0.f);
    for (int i = minX; i <= maxX; ++i) {
        for (int j = minY; j <= maxY; ++j) {
            V2i patch = patches(i, j);
            int patternX = cudaClamp(patch(0) + x - i, 0, pattern.width - 1);
            int patternY = cudaClamp(patch(1) + y - j, 0, pattern.height - 1);
            V4uc patternPix = pattern(patternX, patternY);
            acc(0) += (float)(patternPix(0));
            acc(1) += (float)(patternPix(1));
            acc(2) += (float)(patternPix(2));
        }
    }

    // evaluate average pixel and write it to texture
    float averageMult = 1.f / (float)((maxX - minX + 1) * (maxY - minY + 1));
    V4uc newPix(
        (unsigned char)(acc(0) * averageMult),
        (unsigned char)(acc(1) * averageMult),
        (unsigned char)(acc(2) * averageMult),
        (unsigned char)255);

    texture.write(x, y, newPix);
}

// copies a pixel value from lower resolution to the corresponding pixel in the 2x higher resolution, the same applies to patches.
__global__ void upsampleTextureAndPatches(TexArray2<4, unsigned char> originalTexture, TexArray2<4, unsigned char> upsampledTexture, TexArray2<2, int> originalPatches, TexArray2<2, int> upsampledPatches)
{
    int x = blockIdx.x;
    int y = blockIdx.y;

    if (x >= upsampledTexture.width || y >= upsampledTexture.height)
        return;

    int downX = x >> 1;
    int downY = y >> 1;
    upsampledTexture.write(x, y, originalTexture(downX, downY));
    V2i originalPatch = originalPatches(downX, downY);
    originalPatch[0] = originalPatch[0] * 2;
    originalPatch[1] = originalPatch[1] * 2;
    upsampledPatches.write(x, y, originalPatch);
}

// evaluates average pixel in the lower resolution from corresponding 4 pixels in the 2x higher resolution
__global__ void downsampleTexture(TexArray2<4, unsigned char> originalTexture, TexArray2<4, unsigned char> downsampledTexture)
{
    int x = blockIdx.x;
    int y = blockIdx.y;

    if (x >= downsampledTexture.width || y >= downsampledTexture.height)
        return;

    V3f acc(0, 0, 0);
    for (int i = 2 * x; i < 2 * x + 2; ++i) {
        for (int j = 2 * y; j < 2 * y + 2; ++j) {
            V4uc v = originalTexture(i, j);
            acc[0] += (float)v[0];
            acc[1] += (float)v[1];
            acc[2] += (float)v[2];
        }
    }

    float mult = 0.25f;
    V4uc newPix(
        (unsigned char)(acc[0] * mult),
        (unsigned char)(acc[1] * mult),
        (unsigned char)(acc[2] * mult),
        255u
        );

    downsampledTexture.write(x, y, newPix);
}

#if TEXSYNTH_DEBUG
// evaluates and saves image showing distribution of patches in the pattern
void savePatternPatchesImg(int w, int h, TexArray2<2, int> & patchesGpu, int ord)
{
    A2V4f textureCpu = A2V4f(w, h);
    A2V2i patches(patchesGpu.width, patchesGpu.height);
    A2V1i patchCount(patchesGpu.width, patchesGpu.height);

    copy(&patches, patchesGpu);

    for (int i = 0; i < patchCount.width(); ++i) {
        for (int j = 0; j < patchCount.height(); ++j) {
            patchCount(i, j)[0] = 0;
        }
    }

    for (int i = 0; i < textureCpu.width(); ++i) {
        for (int j = 0; j < textureCpu.height(); ++j) {            
            V4f & v = textureCpu(i, j);
            v(0) = 0.f;
            v(1) = 0.f;
            v(2) = 0.f;
            v(3) = 1.f;
        }
    }

    int maxValue = INT_MIN;
    for (int i = 0; i < patches.width(); ++i) {
        for (int j = 0; j < patches.height(); ++j) {
            V2i patch = patches(i, j);
            patch(0) = cudaClamp(patch(0), 0, patchCount.width() - 1);
            patch(1) = cudaClamp(patch(1), 0, patchCount.height() - 1);
            patchCount(patch)[0] += 1;
            if (patchCount(patch)[0] > maxValue) {
                maxValue = patchCount(patch)[0];
            }
        }
    }

    float frac = 1.0f / (float)maxValue;

    for (int i = 0; i < patches.width(); ++i) {
        for (int j = 0; j < patches.height(); ++j) {
            V2i v = patches(i, j);
            v(0) = cudaClamp(v(0), 0, textureCpu.width() - 1);
            v(1) = cudaClamp(v(1), 0, textureCpu.height() - 1);
            V4f & v1 = textureCpu(v(0), v(1));
            if (v1(0) == 0.f) {
                v1(0) = 1.f;
                v1(1) = 1.f;
                v1(2) = 1.f;
            } else {
                v1(1) -= frac;
                v1(2) -= frac;
            }
        }
    }

    imwrite(textureCpu, "out/patches" + std::to_string(ord) + ".png");
}

// copies texture from gpu to cpu and saves it to file
void saveImgToFile(std::string fileName, TexArray2<4, unsigned char> & texture)
{
    A2V4uc tex(texture.width, texture.height);
    copy(&tex, texture);
    imwrite(tex, fileName);
}
#endif

// runs whole synthesis procedure
void synthesis(A2V4uc & pattern, A2V4uc & texture, int patchSize, int numGpuPatchMatchIters, int numGpuThreadsPerBlock, int refineIters, int levels)
{
    int patchRadius = (patchSize - 1) >> 1;
    int patternWidth = pattern.width();
    int patternHeight = pattern.height();
    int textureWidth = texture.width();
    int textureHeight = texture.height();

    // allocate pattern pyramid on cpu
    TexArray2<4, unsigned char> ** patternsGpu = new TexArray2<4, unsigned char>*[levels];
    // allocate pyramid's highest level on gpu and copy input pattern into it
    patternsGpu[levels - 1] = new TexArray2<4, unsigned char>(patternWidth, patternHeight);
    copy(patternsGpu[levels - 1], pattern);

    curandState * gpuRngStates = initGpuRng(textureWidth, textureHeight);
    checkCudaError(cudaDeviceSynchronize());

    // allocate memory for each pyramid level on gpu and create downsampled patterns for each
    for (int i = 0; i < (levels - 1); ++i) {
        textureWidth = textureWidth >> 1;
        textureHeight = textureHeight >> 1;
        patternWidth = patternWidth >> 1;
        patternHeight = patternHeight >> 1;

        patternsGpu[levels - i - 2] = new TexArray2<4, unsigned char>(patternWidth, patternHeight);
        dim3 patternDim(patternWidth, patternHeight);
        downsampleTexture<<<patternDim,1>>>(*patternsGpu[levels - i - 1], *patternsGpu[levels - i - 2]);
        checkCudaError(cudaDeviceSynchronize());

#if TEXSYNTH_DEBUG
        saveImgToFile("out/patternPyramid" + std::to_string(levels - i - 2) + ".png", *patternsGpu[levels - i - 2]);
#endif
    }

    TexArray2<4, unsigned char> textureGpu(textureWidth, textureHeight);
    TexArray2<2, int> patchesGpu(textureWidth, textureHeight);
    TexArray2<2, int> patches2Gpu(textureWidth, textureHeight);
    TexArray2<1, float> eGpu(textureWidth, textureHeight);
    dim3 textureDim(textureWidth, textureHeight);    

    // initialize random patches for the texture (whole patch should fit inside the pattern)
    initRandomPatches<<<textureDim,1>>>(patchesGpu, UINT_MAX / (patternWidth - patchSize + 1), UINT_MAX / (patternHeight - patchSize + 1), patchRadius, gpuRngStates);
    checkCudaError(cudaDeviceSynchronize());

    // loop through all levels of texture synthesis, starting from the lowest
    for (int i = 0; i < levels; ++i) {
        // loop current level refinement
        adjustTexture<<<textureDim,1>>>(textureGpu, *patternsGpu[i], patchesGpu, patchRadius);
        checkCudaError(cudaDeviceSynchronize());
        printf("Refining resolution %dx%d\n", textureWidth, textureHeight);
        for (int j = 0; j < refineIters - 1; ++j) {
            patchmatchGPU(textureGpu, *patternsGpu[i], patchSize, numGpuPatchMatchIters, numGpuThreadsPerBlock, patchesGpu, patches2Gpu, eGpu, gpuRngStates);
            adjustTexture<<<textureDim,1>>>(textureGpu, *patternsGpu[i], patchesGpu, patchRadius);
            checkCudaError(cudaDeviceSynchronize());
        }

#if TEXSYNTH_DEBUG
        saveImgToFile("out/out" + std::to_string(i) + ".png", textureGpu);
        savePatternPatchesImg(patternsGpu[i]->width, patternsGpu[i]->height, patchesGpu, i);
#endif

        // finish in case the last level has been refined
        if (i == levels - 1)
            break;

        // upsample and prepare data for next level refinement
        textureWidth = textureWidth << 1;
        textureHeight = textureHeight << 1;
        textureDim = dim3(textureWidth, textureHeight);
        TexArray2<4, unsigned char> upsampledTextureGpu(textureWidth, textureHeight);
        TexArray2<2, int> upsampledPatchesGpu(textureWidth, textureHeight);
        patches2Gpu = TexArray2<2, int>(textureWidth, textureHeight);
        eGpu = TexArray2<1, float>(textureWidth, textureHeight);

        upsampleTextureAndPatches<<<textureDim,1>>>(textureGpu, upsampledTextureGpu, patchesGpu, upsampledPatchesGpu);
        checkCudaError(cudaDeviceSynchronize());
        textureGpu = upsampledTextureGpu;
        patchesGpu = upsampledPatchesGpu;
        patchmatchGPU(textureGpu, *patternsGpu[i + 1], patchSize, numGpuPatchMatchIters, numGpuThreadsPerBlock, patchesGpu, patches2Gpu, eGpu, gpuRngStates);

#if TEXSYNTH_DEBUG
        saveImgToFile("out/upsampled" + std::to_string(i) + ".png", upsampledTextureGpu);
#endif
    }

    // release allocated cpu and gpu memory
    cudaFree(gpuRngStates);
    for (int i = 0; i < levels; ++i)
        delete patternsGpu[i];
    delete[] patternsGpu;

    // copy synthesised texture from gpu to cpu
    copy(&texture, textureGpu);
}

// checks if value (resolution width/height) can maintain precise integer value on all downsampled levels and the lowest level is still larger or equal to the patch size
bool areLevelsValid(int value, int levels, int patchSize) {
    if (value <= 0)
        return false;
    for (int i = 0; i < levels; ++i) {
        if (value % 2 == 1)
            return false;
        if (i < levels - 1)
            value = value >> 1;
    }
    return value >= patchSize;
}

// validate all user inputs
bool validateInput(int outputWidth, int outputHeight, int patternWidth, int patternHeight, int patchSize, int patchmatchIters, int patchmatchThreadsPerBlock, int refinementIters, int pyramidLevels) {
    return
        patchmatchIters > 0 &&
        patchmatchThreadsPerBlock > 0 &&
        refinementIters > 0 &&
        pyramidLevels > 0 &&
        (patchSize % 2) == 1 &&
        areLevelsValid(outputWidth, pyramidLevels, patchSize) &&
        areLevelsValid(outputHeight, pyramidLevels, patchSize) &&
        areLevelsValid(patternWidth, pyramidLevels, patchSize) &&
        areLevelsValid(patternHeight, pyramidLevels, patchSize);
}

int main(int argc, char** argv)
{
    int outputWidth = 512;
    int outputHeight = 512;
    int patchSize = 7;
    int patchmatchIters = 6;
    int patchmatchThreadsPerBlock = 16;
    int refinementIters = 20;
    int pyramidLevels = 5;
    std::string inputPatternFile = "in/straws128x128.jpg";
    std::string synthesizedTextureFile = "out/out.png";

    A2V4uc inputPattern = imread<V4uc>(inputPatternFile);
    A2V4uc synthesizedTexture(outputWidth, outputHeight);

    if (!validateInput(outputWidth, outputHeight, inputPattern.width(), inputPattern.height(), patchSize, patchmatchIters, patchmatchThreadsPerBlock, refinementIters, pyramidLevels)) {
        printf("Invalid input.\n");
        return;
    }
    timerReset();
    synthesis(inputPattern, synthesizedTexture, patchSize, patchmatchIters, patchmatchThreadsPerBlock, refinementIters, pyramidLevels);
    printf("Elapsed milliseconds: %f\n", timerGet());
    imwrite(synthesizedTexture, synthesizedTextureFile);
}